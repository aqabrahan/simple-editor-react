
class Synonym {
  static getSynonyms(word) {
    return fetch(`https://api.datamuse.com/words?sl=${word}`)
    .then(response => response.json())
    .then(data => {
      let options = [];
      if(data.length > 0) {
        data.map( syn => {
          options.push({value: syn.word,label: syn.word});
        })
      }
      return options;
    })
  }

}

module.exports = Synonym;