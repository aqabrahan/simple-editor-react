import React, { Component } from 'react';
import './ControlPanel.css';
import Service from '../services/synonym';

class ControlPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: null,
            options: [],
            showLoading: false,
            showSyns: false
        }
        this.onGetSynonyms = this.onGetSynonyms.bind(this);
        this.onSelect = this.onSelect.bind(this);
        this.onCloseSynonyms = this.onCloseSynonyms.bind(this);
    }
    onExecuteFormat(format) {
        this.props.onApplyFormat(format);
    }
    onGetSynonyms() {
        const word = window.getSelection().toString();
        if (word !== '') {
            this.setState({
                showLoading: true,
                showSyns: false,
                options: []
            });
            Service.getSynonyms(word)
            .then(data => {
                this.setState({
                    options: data,
                    showLoading: false,
                    showSyns: true
                })
            })
            .catch(() => {
                this.setState({
                    showLoading: false
                })
            })
        }
    }
    onSelect = value => {
        let range;
        const sel = window.getSelection();
        if (sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();
            range.insertNode(document.createTextNode(value));
        }
    }
    onCloseSynonyms = () => {
        this.setState({
            showSyns: false
        });
    }
    render() {
        const { disableTools } = this.props;
        const { options } = this.state;
        return (
            <div id="control-panel">
                <div id="format-actions">
                    <button className="format-action" type="button" disabled={disableTools} onClick={() => this.onExecuteFormat('bold')}><b>B</b></button>
                    <button className="format-action" type="button" disabled={disableTools} onClick={() => this.onExecuteFormat('italic')}><i>I</i></button>
                    <button className="format-action" type="button" disabled={disableTools} onClick={() => this.onExecuteFormat('underline')}><u>U</u></button>
                    <button className="format-action" type="button" disabled={disableTools} onClick={() => this.onGetSynonyms()}>Change by synonym</button>
                    {this.state.showLoading && <span>Loading Synonyms...</span>}
                    {this.state.showSyns && <div className="list">
                        <button className="btn-close" onClick={() => this.onCloseSynonyms()}>X</button>
                        <ul className="list-items">
                            {options.length > 0 && options.map((op, i) => {
                                return <li key={`listitem${i}`} className="list-item"><button className="btn-syn" onClick={() => this.onSelect(op.value)}>{op.value}</button></li>;
                            })}
                        </ul>
                    </div>
                    }
                </div>
            </div>
        );
    }
}

export default ControlPanel;
