import React, { Component } from 'react';
import './FileZone.css';
import Editor from './Editor';

class FileZone extends Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.state = {
            html: ''
        }
    }
    onChange(event) {
        this.setState({
            html: event.target.value,
            words: this.getWords(event.target.value)
        });
        this.props.onSelect();
    }
    getWords(text) {
        let texts = [];
        if (text !== "") {
            texts = text.split(' ');
        }
        return texts;
    }

    render() {
        return (
            <div id="file-zone">
                <div id="file">
                    <Editor html={this.state.html} onChange={this.onChange} onSelect={this.props.onSelect} />
                </div>
            </div>
        );
    }
}

export default FileZone;
