import React, { Component } from 'react';
import ReactDom from 'react-dom';

class Editor extends Component {
    constructor(props) {
        super(props);
        this.onChangeHandler = this.onChangeHandler.bind(this);
    }
    shouldComponentUpdate(nextProps) {
      return nextProps.html !== ReactDom.findDOMNode(this).innerHTML;
    }
    onChangeHandler() {
      const html = ReactDom.findDOMNode(this).innerHTML;
      if (this.props.onChange && html !== this.lastHtml) {
          this.props.onChange({
              target: {
                  value: html
              }
          });
      }
      this.lastHtml = html;
    }

    render() {
        return (
          <div
            id="diveditor"
            className="diveditor"
            suppressContentEditableWarning={true}
            onInput={this.onChangeHandler}
            onBlur={this.onChangeHandler}
            contentEditable
            spellCheck={false}
            onMouseUp={this.props.onSelect}
            dangerouslySetInnerHTML={{__html: this.props.html}}>

          </div>
        );
    }
}

export default Editor;
