import React, {Component} from 'react';
import './App.css';
import ControlPanel from "./control-panel/ControlPanel";
import FileZone from "./file-zone/FileZone";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            disableTools: true
        }
        this.onSelect = this.onSelect.bind(this);
    }
    onApplyFormat(applyFormat) {
        if (window.getSelection().toString() !== '') {
            document.execCommand(applyFormat,false, window.getSelection().toString())
        }
    }
    onSelect() {
        if (window.getSelection().toString() !== '') {
            this.setState({
                disableTools: false,
            });
        } else {
            this.setState({
                disableTools: true
            })
        }
    }
    render() {
        return (
            <div className="App">
                <header>
                    <span>Simple Text Editor</span>
                </header>
                <main>
                    <ControlPanel onApplyFormat={this.onApplyFormat}  disableTools={this.state.disableTools} />
                    <FileZone onSelect={this.onSelect} />
                </main>
            </div>
        );
    }
}

export default App;
