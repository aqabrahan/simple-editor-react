# Simple text editor

## Initial setup
Run `npm install` in order to setup application

## Development server
Run `npm start` for a dev server.

## Steps to run the functionality
1. Write text in the text field
2. Double-click in a word to apply bold, italic, underline.
3. To change for Synonyms, double-click in a word, click in button `Change by synonym`, it show a list of word's synonym, of this list select one to change, to close the list click in button "X"


